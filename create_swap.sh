#!/bin/bash
export PATH=/bin:/usr/bin:/usr/local/bin

echo "Creating swap memory as per your requirement on ubuntu 20.04"
echo "============================================================"
echo "============================================================"
read -p "Enter swap memory as per your requirement eg: 8,16,32: " swapmemory
read -p "Enter swapfile name eg: swapfile: " swapfile
swapfile=${swapfile:-swapfile}
echo "$swapfile"

#for confirmation
read -p "Continue? (Y/N): " confirm && [[ $confirm == [yY] || $confirm == [yY][eE][sS] ]] || exit 1

echo "creating a swap file with the fallocate program. This command instantly creates a file of the specified size."
sudo fallocate -l "$swapmemory"G /"$swapfile"
echo "============================================================"

echo "Making the $swapfile only accessible to root"
sudo chmod 600 /"$swapfile"
echo "============================================================"

echo "Marking the $swapfile as swap space:"
sudo mkswap /"$swapfile"
echo "============================================================"

echo "After marking the $swapfile, now enabling the $swapfile, allowing our system to start using it:"
sudo swapon /"$swapfile"
echo "============================================================"

echo "Verifying that the swap is available "
sudo swapon --show
echo "============================================================"

echo "The output of the free utility to corroborate our findings:"
free -h
echo "============================================================"

echo "creating a backup of fstab file"
sudo cp /etc/fstab /etc/fstab.bak
echo "============================================================"

echo "Making the Swap File Permanent"
echo '/swapfile none swap sw 0 0' | sudo tee -a /etc/fstab
echo "============================================================"

echo "=== Congratulations!! you have successfully created swap memory of "$swapmemory"G" with the filename as /"$swapfile. ==="